path = require('path')
url = require('url')
_ = require('lodash')
request = require('request')
Promise = require('bluebird')
uuid = require('node-uuid')

class Client

  @waitForConnection: (host = 'http://localhost:4100') ->
    new Promise (resolve, reject) =>
      checkHealth = =>
        request method: 'get', url: host, json: true, (err, result) ->
          if result?.body?.status is 'ok'
            resolve()
          else
            setTimeout(checkHealth, 100)
      checkHealth()

  options:
    host: 'http://localhost:4100'

  constructor: (options) ->
    if not (this instanceof Client)
      return new Client(options)
    if not options?.app?
      throw new Error('You must specify an app to connect to')
    @options = _.defaults(options, @options)

  req: (options) ->
    new Promise (resolve, reject) =>
      options.json ?= true
      options.headers ?= {}
      options.headers.Authorization = 'AUTHTOKEN ' + @options.activeToken
      options.headers.Host = @options.host.replace(/http(s)?:\/\//, @options.app + '.')
      request options, (err, result) ->
        if err or result.statusCode >= 500
          console.error('Error making request:')
          console.error(options)
          reject(err or result.body.replace(/<br>/g, '\n').replace(/&nbsp;/g, '  '))
        else
          resolve(result)

  endpoint: (endpointPath) ->
    url.resolve(@options.host, endpointPath)

  healthcheck: ->
    @req(method: 'get', url: @endpoint('/'))

  signup: (username, password) ->
    if not username? and not password?
      username = uuid()
      password = uuid()
    @req
      method: 'post'
      url: @endpoint('/data/authentications')
      body: { username, password }

  login: (username, password) ->
    if not username? and not password?
      username = uuid()
      password = uuid()
      user = @signup().then -> return { username, password }
    else
      user = Promise.resolve({ username, password })
    user.then (credentials) =>
      @req
        method: 'post'
        url: @endpoint('/data/authtokens')
        body: credentials
    .then (token) ->
      @options.activeToken = token

  logout: ->
    @destroy('authtokens', @options.activeToken.id)
    .then =>
      @options.activeToken = null

  get: (table, id) ->
    @req
      method: 'get'
      url: @endpoint('/data/' + table + '/' + id)

  save: (table, id, body) ->
    if id?
      method = 'put'
      url = @endpoint('/data/' + table + '/' + id)
    else
      method = 'post'
      url = @endpoint('/data/' + table)
    @req
      method: method
      url: url
      body: body

  destroy: (table, id) ->
    @req
      method: 'delete'
      url: @endpoint('/data/' + table + '/' + id)

  rpc: (name, body) ->
    @req
      method: 'post'
      url: @endpoint('/rpc/' + name)
      body: body

module.exports = Client